# Ansible role to create cross-environments

Main idea is to have cross-environment similar to host system
with the same set of repositories and configuration.
This is most convenient way to have 32-bit applications on 64-bit host,
especially with conditional package options to make cross-env smaller and simpler
(see next paragraph).

Role will only create necessary directories and configuration files.
To build toolchain follow instructions[1] or use example script[2].
**NB** Every `cave` call with `-mx` should be replaced with `cave -E :env_name`.

[1]: https://exherbo.org/docs/multiarch/multiarch-new-target.html
[2]: https://gitlab.exherbo.org/marv/infra-scripts/-/blob/cross-host-bootstrap-script/cross/bootstrap-cross-host.sh

## Usage example
Repository can be submoduled under `roles` directory and used in playbook as:
```
roles:
- role: paludis-prepare-cross
  tags:
    - i686
  vars: # optional
    cross_env_name: i686
    cross_compile_host: i686-pc-linux-gnu
```

## Conditional package options
`options.conf.d/simple_cross.bash` example:

```
if [[ -n "${CAVE_CMDLINE_environment}" ]]; then
cat <<EOF
*/* -X
EOF
fi
```

